== Lista de comida

// Ordenada por orden alfabético en cada apartado

=== Aperitivos

* Aceitunas con anchoas
* Jamón ibérico en lonchas
* Nachos con queso
* Patatas fritas

=== Platos principales

* Arroz
* Chorizo criollo
* Chuletas de cabeza
* Pollo
* Secreto ibérico
* Wagyu

=== Postres

* Arroz con leche
* Crema catalana
* Tarta de queso
